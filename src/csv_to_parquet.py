import os
from pyspark.sql import SparkSession , SQLContext
from pyspark.sql import functions as functions

# bucket name 
BUCKET_NAME="terraform-spark"

# folder bucket 

BUCKET_FOLDER_CSV="transfert.csv"

spark=SparkSession.builder.appName("PySpark").getOrCreate()

sc=spark.sparkContext

sql_c=SQLContext(spark.sparkContext)

print("Loadind CSV files.....")

data=sql_c.read.csv(

    os.path.join("gs://{}".format(BUCKET_NAME),BUCKET_FOLDER_CSV,"*.csv"),

    header=True,
    sep=","
)

print("Exporting to Parquet file ...")

data.write.parquet(os.path.join("gs://{}".format(BUCKET_NAME),BUCKET_FOLDER_CSV,"output.parquet"))



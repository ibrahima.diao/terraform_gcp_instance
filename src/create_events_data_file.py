
#! usr/bin/python
# -*- coding: ISO-8859-1 -*-
# [START dataproc_pyspark_bigquery]
"""
Ce code Pyspark a plusieurs fonctions.
Il va inscrire dans le Data Lake (BigQuery) tous les trajets normalisés effectués.
Connecteurs Spark :
- BigQuery : gs://spark-lib/bigquery/spark-bigquery-latest_2.12.jar
! Cela va prendre plus de deux heures !
"""

import pyspark.sql.functions as func

from pyspark.sql import SparkSession, SQLContext
import os
import sys

from pyspark.sql import functions as func
from pyspark.sql import SparkSession
from pyspark.sql.window import Window

from datetime import datetime


# bucket name 

BUCKET_NAME = "terraform-spark"
BUCKET = "gs://{}".format(BUCKET_NAME)


# La table où seront stockés les trajets
TARGET_TABLE = "datalake.tripstest"


spark = SparkSession \
    .builder \
    .appName("PySpark") \
    .getOrCreate()

sc = spark.sparkContext
sql_c = SQLContext(spark.sparkContext)

data = sql_c.read.parquet(
    BUCKET + "/transfert.csv/output.parquet",
    header=True,
    sep=",")
print("Normalizing ...")

print("Computing product-related variables ...")
data_sessions = data.filter(func.col("user_session").isNotNull())

events_per_session = data_sessions \
    .withColumn(
        'purchased',
        func.when(func.col('event_type') == 'purchase', 1).otherwise(0)
    ) \
    .withColumn(
        'num_views',
        func.when(func.col('event_type') == 'view', 1).otherwise(0)
    ) \
    .groupBy(["user_session", "product_id"]) \
    .agg(
        func.max('purchased').alias('purchased'),
        func.sum('num_views').alias('num_views_product'),
    )

views_per_session = data \
    .withColumn(
        'num_views',
        func.when(func.col('event_type') == 'view', 1).otherwise(0)
    ) \
    .groupBy(["user_session"]) \
    .agg(
        func.sum('num_views').alias('num_views_session')
    )
events_per_session = events_per_session.join(
    views_per_session,
    on=["user_session"],
    how="inner"
)

data_events = data_sessions.join(
    events_per_session,
    on=["user_session", "product_id"],
    how="inner"
)

data_events = data_events \
    .withColumn('hour', func.hour("event_time")) \
    .withColumn('minute', func.minute("event_time")) \
    .withColumn('weekday', func.dayofweek("event_time") - 2) \
    .withColumn("category", func.split(func.col("category_code"), r"\.").getItem(0)) \
    .withColumn("sub_category", func.split(func.col("category_code"), r"\.").getItem(1))

data_events = data_events \
    .withColumn(
        'weekday',
        func.when(func.col('weekday') == -1, 6).otherwise(func.col("weekday"))
    )

print("Computing sessions-related variables ...")
sessions_duration = data_events \
    .groupBy(["user_session"]) \
    .agg(
        func.min('event_time').alias('amin'),
        func.max('event_time').alias('amax')
    ) \
    .withColumn(
        "duration",
        func.col("amax").cast("int") - func.col("amin").cast("int")
    )

dataset = data_events \
    .sort("event_time") \
    .dropDuplicates(["event_type", "product_id", "user_id", "user_session"]) \
    .filter(func.col("event_type").isin(["cart", "purchase"])) \
    .join(
        sessions_duration.select(["user_session", "duration"]),
        on=["user_session"],
        how="inner"
    )

count_prev_sessions = dataset \
    .select(["user_id", "user_session", "event_time"]) \
    .withColumn( # Spark ne préserve pas l'ordre après le groupBy
        "event_time", # On affecte pour chaque événement de la session le timestamp du début de session
        func.first("event_time").over(Window.partitionBy(["user_id", "user_session"]).orderBy("event_time"))
    ) \
    .groupBy(["user_id", "user_session"]) \
    .agg(
        func.first("event_time").alias("event_time")
    ) \
    .withColumn(
        "num_prev_sessions",
        func.row_number().over(Window.partitionBy(["user_id"]).orderBy("event_time")) - 1
    )

dataset = dataset.join(
    count_prev_sessions.select(["user_session", "num_prev_sessions"]),
    on=["user_session"],
    how="inner"
)

view_prev_session = dataset \
    .select(["user_id", "user_session", "event_time", "product_id"]) \
    .withColumn( # Même principe car l'ordre du orderBy n'est pas préservé
        "event_time",
        func.first("event_time").over(
            Window.partitionBy(["user_id", "user_session", "product_id"]).orderBy("event_time")
        )
    ) \
    .groupBy(["user_id", "user_session", "product_id"]) \
    .agg(
        func.first("event_time").alias("event_time")
    ) \
    .withColumn(
        "num_prev_product_views",
        func.row_number().over(Window.partitionBy(["user_id", "product_id"]).orderBy("event_time")) - 1
    )

dataset = dataset.join(
    view_prev_session.select(["user_session", "product_id", "num_prev_product_views"]),
    on=["user_session", "product_id"],
    how="inner"
)

dataset = dataset \
    .sort("event_time") \
    .dropDuplicates(["user_session", "product_id", "purchased"]) \
    .select([c for c in dataset.columns if c not in \
             {"event_time", "event_type", "category_code", "category_id"}
    ])

# Le bucket de transition
spark.conf.set('temporaryGcsBucket', BUCKET_NAME)

# Écriture 
dataset.write.format('bigquery') \
    .option('table', TARGET_TABLE) \
    .save()





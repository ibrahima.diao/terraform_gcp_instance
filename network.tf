
###


resource "google_compute_network" "dataproc_network" {
  name                    = "dataproc-network"
  auto_create_subnetworks = true
}
resource "google_compute_firewall" "dataproc_allow_internal" {
  name    = "dataproc-allow-internal"
  network = google_compute_network.dataproc_network.self_link

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }

  source_ranges = ["10.128.0.0/9"]
   target_tags   = ["dataproc"]
}
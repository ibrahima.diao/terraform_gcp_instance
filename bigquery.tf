resource "google_bigquery_dataset" "datasets" {
  count = length(var.dataset_id_with_description)

  dataset_id = var.dataset_id_with_description[count.index]["id"]
  description = var.dataset_id_with_description[count.index]["description"]
  location = "europe-west1"

}

resource "google_bigquery_table" "example" {
  dataset_id = "dataset_t"
  table_id   = "my_table"
  project    = "vote-for-innov"

  schema = <<EOF
[
  {
    "name": "name",
    "type": "STRING",
    "mode": "REQUIRED"
  },
  {
    "name": "age",
    "type": "INTEGER",
    "mode": "REQUIRED"
  }
]
EOF
depends_on = [
    google_bigquery_dataset.datasets
  ]
}
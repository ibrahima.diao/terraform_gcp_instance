# Define input variable for a terraform module

variable "project_id" {

    default="vote-for-innov"
}

variable "region" {

    default = "europe-west1"
}

variable "dataset_id_with_description" {
  description = "A list of all the dataset ID's and their descriptions."
  
  default = [
    {
      id          = "dataset_t"
      description = "The first dataset."
    },
    {
      id          = "dataset_te"
      description = "The second dataset."
    },
  ]
}

locals {
  cloud_composer = {
  name = "cloud-composer-${var.project_id}"
  region = "europe-west1"
  node_count = 3
  zone = "europe-west1"
  machine_type = "n1-standard-1"
}
}


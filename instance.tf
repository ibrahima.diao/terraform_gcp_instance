

resource "google_dataproc_cluster" "cluster" {
  name    = "test-dataproc-cluster"
  region  ="europe-west1"

  cluster_config {
    master_config {
      num_instances = 1
      machine_type  = "n1-standard-2"
    }

    worker_config {
      num_instances = 2
      machine_type  = "n1-standard-2"
    }

    software_config {
      image_version = "2.0.27-debian10"
    
    }

    gce_cluster_config {
      network = google_compute_network.dataproc_network.self_link
      service_account_scopes = [
        "https://www.googleapis.com/auth/cloud-platform",
      ]

      tags = ["dataproc"]
    }
  }

}

## cloud composer 



resource "google_composer_environment" "cloud-composer" {

   name = "${local.cloud_composer["name"]}"
   region = "${local.cloud_composer["region"]}"
   config {
    software_config {
      image_version = "composer-1.20.12-airflow-1.10.15"
    }
  }

  
   
}